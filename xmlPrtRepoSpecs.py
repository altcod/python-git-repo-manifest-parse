#!/usr/bin/env python
#  xmlPrtRepoSpecs.py

import xml.dom.minidom as dom
import xml.dom.minicompat as compat
import sys
import os
import subprocess

class RepoCollect:
    def __init__(self, fn=None):
        self._results = []
        if fn is None:
            return
        test = os.path.isfile(fn)
        if not test:
            fn = None

        if fn is not None:
            dom1 = dom.parse(fn)

            # print dom1.toxml()
            self._results = []
            self.aggregateChildren(dom1, self._results)

    def aggregateChildren(self, doc, retl=None, nest=0):
        nAttr = 0
        if type(doc.attributes) is dom.NamedNodeMap:
            nAttr = doc.attributes.length
        elif doc.attributes is not None:
            raise Exception("doc attr unsupported %s" % str(type(doc.attributes)))
        nChild = 0
        if type(doc.childNodes) is compat.NodeList:
            nChild = doc.childNodes.length
        elif type(doc.childNodes) is not compat.EmptyNodeList:
            raise Exception("doc child unsupported %s" % str(type(doc.childNodes)))
        retv = []
        if retl is not None:
            retv = retl

        #print "%s localname %s  nattr %d nchild %d\n" %( " " * 4 * nest, doc.localName, nAttr, nChild)
        if doc.localName is not None:
            attrs = {}
            attrkeys = []
            for k in doc.attributes.keys():
                v = doc.attributes[k].nodeValue
                attrs[k] = v
                attrkeys.append(k)
            retv.append([nest, doc.localName, attrkeys, attrs])

        for i in range(0, nChild):
            self.aggregateChildren(doc.childNodes[i], retv, nest + 1)

    def prtResults(self):
        for v in self._results:
            print v

    def listProjs(self):
        repos = [] # each: path, revision, name
        for v in self._results:
            node = v[1]
            attks = v[2]
            attrs = v[3]
            if node != u'project':
                continue
            if u'path' in attks and u'name' in attks and u'revision' in attks:
                repo = {}
                repo['path'] = attrs['path']
                repo['name'] = attrs['name']
                repo['revision'] = attrs['revision']
                repos.append(repo)
                continue
            raise Exception("Unknown type of project node: attrs %s" % str(attrs))
        self._projs = repos
        return repos

    def getHashBranchInfo(self):
        for i in range(0, len(self._projs)):
            cmd = "cd %s && (git log -1 --pretty=oneline | sed -e s/\ .*//)" % self._projs[i]['path']
            hh = subprocess.check_output(['bash', '-c', cmd])
            h = hh.rstrip()
            cmd = "cd %s && git branch -a --contains %s" % (self._projs[i]['path'], h)
            br = subprocess.check_output(['bash','-c',cmd])
            self._projs[i]['hash'] = h
            self._projs[i]['brch'] = br

    def prtProjectSpec(self):
        for p in self._projs:
            print "project: %s %s %s" % (p['path'], p['hash'], p['name'])
            brs = p['brch'].split('\n')
            for n in brs:
                print "    %s" % n
                pass

if __name__ == "__main__":
    fname = None
    args = sys.argv
    argc = len(args)
    if type(args) is list and argc >= 2:
        fname = args[1]
        test = os.path.isfile(fname)
        if not test:
            fname = None
    if fname is not None:
        gen = RepoCollect(fname)
        gen.listProjs()
        gen.getHashBranchInfo()
        gen.prtProjectSpec()
        pass


