#!/usr/bin/env python
#  diffTails.py

import sys
import os
import subprocess

if __name__ == "__main__":
    fn1, fn2 = None, None
    args = sys.argv
    argc = len(args)
    if type(args) is list and argc >= 3:
        fn1, fn2 = args[1], args[2]
        test1 = os.path.isfile(fn1)
        test2 = os.path.isfile(fn2)
        if not test1 or not test2:
            fn1, fn2 = None, None
    if fn1 is not None and fn2 is not None:
        fn11 = fn1 + "-tail"
        fn22 = fn2 + "-tail"
        def getTail(fnIn, fnOut):
            with open(fnIn, "r") as f_in:
                f_in.readline(); f_in.readline() # skip two lines
                tail = f_in.read()
                f_in.close()
                with open(fnOut, "w") as f_out:
                    f_out.write(tail)
                    f_out.close()
        getTail(fn1, fn11), getTail(fn2, fn22)
        cmd = "diff -au %s %s > %s-changes" % (fn11, fn22, fn2)
        try:
            hh = subprocess.check_output(['bash', '-c', cmd])
        except:
            hh = None
        if hh is None:
            cmd = "diff -au %s %s > %s-changes" % (fn1, fn2, fn2)
            try:
                hh = subprocess.check_output(['bash', '-c', cmd])
            except:
                hh = None
        if hh is None:
            sys.exit(1)
    sys.exit(0)



