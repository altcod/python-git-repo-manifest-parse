
import xml.dom.minidom as dom
import xml.dom.minicompat as compat

dom1 = dom.parse("prjs_manifests/the_build/layout.xml")

def aggregateChildren(doc, retl=None, nest=0):
    nAttr = 0
    if type(doc.attributes) is dom.NamedNodeMap:
        nAttr = doc.attributes.length
    elif doc.attributes is not None:
        raise Exception("doc attr unsupported %s" % str(type(doc.attributes)))
    nChild = 0
    if type(doc.childNodes) is compat.NodeList:
        nChild = doc.childNodes.length
    elif type(doc.childNodes) is not compat.EmptyNodeList:
        raise Exception("doc child unsupported %s" % str(type(doc.childNodes)))
    retv = []
    if retl is not None:
        retv = retl

    print "%s localname %s  nattr %d nchild %d\n" %( " " * 4 * nest, doc.localName, nAttr, nChild)
    if doc.localName is not None:
        attrs = []
        for k in doc.attributes.keys():
            v = doc.attributes[k].nodeValue
            kv = {}
            kv[k] = v
            attrs.append(kv)
        retv.append([nest, doc.localName, attrs])

    for i in range(0, nChild):
        aggregateChildren(doc.childNodes[i], retv, nest + 1)

#print dom1.toxml()
result = []
aggregateChildren(dom1, result)
for v in result:
    print v

