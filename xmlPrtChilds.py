
import xml.dom.minidom as dom
import xml.dom.minicompat as compat

dom1 = dom.parse("prjs_manifests/the_build/layout.xml")

def prtChildren(doc, nest=0):
    nAttr = 0
    if type(doc.attributes) is dom.NamedNodeMap:
        nAttr = doc.attributes.length
    elif doc.attributes is not None:
        raise Exception("doc attr unsupported %s" % str(type(doc.attributes)))
    nChild = 0
    if type(doc.childNodes) is compat.NodeList:
        nChild = doc.childNodes.length
    elif type(doc.childNodes) is not compat.EmptyNodeList:
        raise Exception("doc child unsupported %s" % str(type(doc.childNodes)))
    print "%s localname %s  nattr %d nchild %d\n" %( " " * 4 * nest, doc.localName, nAttr, nChild)
    for i in range(0, nChild):
        prtChildren(doc.childNodes[i], nest + 1)

#print dom1.toxml()
prtChildren(dom1)

